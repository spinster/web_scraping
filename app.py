import os
from flask import Flask, render_template, jsonify, redirect
import pymongo
from bson.objectid import ObjectId
import scrape_mars

app = Flask(__name__)

dbuser = os.environ.get('dbuser')
dbpassword = os.environ.get('dbpassword')
conn = f'mongodb://{dbuser}:{dbpassword}@ds231739.mlab.com:31739/mars'
client = pymongo.MongoClient(conn)
db = client.mars
collection = db.mars


@app.route('/')
def index():
	mars = db.collection.find_one()
	return render_template('index.html', mars=mars)


@app.route('/scrape')
def scrape():
	mars = db.collection
	mars_data = scrape_mars.scrape()
	mars.update_one(
		{'_id': ObjectId('5afb27cecb760ed93b01b792')},
		{'$set': mars_data},
		upsert=True
    )
    return redirect("/", code=302)


if __name__ == "__main__":
    app.run(debug=False)
