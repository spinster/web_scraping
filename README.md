# Mission to Mars

Code for the actual assignment was completed by Mae Ling Mak with the aid of the Trilogy assigned
tutor, Dennis Tran (dennistran714@gmail.com).

Heroku deployment was achieved by nearly endless hours of documentation from Heroku and Stack Overflow,
along with hints from Matthew Young and a non-Trilogy tutor.

This disclaimer is included for personal progress tracking and to remain GPL compliant.

"Credit where credit's due." -Randall Graves, "Clerks"

The Heroku app can be found at: https://missiontomars.herokuapp.com/

As of 05/15/2018, error 503 crashes upon running.

Will fix and update at a later date.

spinster@Tue May 15 14:55:36 PDT 2018

