# Dependencies
import os
from bs4 import BeautifulSoup as bs
from splinter import Browser
import pandas as pd
import time
import random
from selenium import webdriver

#failsafe
from splinter.exceptions import ElementDoesNotExist

# Create a Beautiful Soup object & load chromedriver
def init_browser():
    driver_path = os.environ.get('GOOGLE_CHROME_SHIM', None)
    chrome_options = webdriver.ChromeOptions()
    chrome_options.binary_location = driver_path
    chrome_options.add_argument('no-sandbox')
    chrome_options.add_argument('--headless')
    return Browser('chrome', executable_path="chromedriver", options=chrome_options, headless=True)


def scrape():
    browser = init_browser()
    mars = {}

    #Mars news url
    url = 'https://mars.nasa.gov/news/'
    #visit the url
    browser.visit(url)
    sleep(random.random()*9)

#set up html and the soup
    html = browser.html
    soup = bs(html, 'lxml')

    #Grab headlines and paragraph teaser
    container = soup.find('div', class_='list_text')
    news_title = container.find(class_='content_title')
    teaser_body = container.find(class_='article_teaser_body')

    #Get latest news title
    latest_title = news_title.get_text()
    print(latest_title)

    #Get teaser paragraph
    paragraph = (teaser_body.get_text())
    print(paragraph)

    #Grab images

    #Featured image of the day
    url = 'https://www.jpl.nasa.gov/spaceimages/?search=&category=Mars'

    #visit img url & saving the html
    browser.visit(url)
    time.sleep(random.random()*9)
    featured_image = browser.find_by_id('full_image')
    featured_image.click()
    more_info = browser.find_link_by_partial_text('more info')
    html=browser.html
    img_soup = bs(html,'lxml')

    #featured image
    featured_image = img_soup.find('a', class_='fancybox')

    featured_image = featured_image['data-link']

    #save full URL to featured image
    featured_image_link = f'https://www.jpl.nasa.gov{featured_image}'
    print(featured_image_link)

	#get the actual image
    browser.visit(featured_image_link)
    time.sleep(random.random()*9)
    html=browser.html
    img_soup = bs(html,'lxml')

    featured_image=img_soup.find('figure', class_='lede')
    find_the_thing = featured_image.find('a')['href']
    featured_image_link = f'https://www.jpl.nasa.gov{find_the_thing}'
    print(featured_image_link)


    #Get tweet from Mars weather Twitter account
    url = 'https://twitter.com/marswxreport?lang=en'
    browser.visit(url)
    time.sleep(random.random()*9)
    html = browser.html
    tweet_soup = bs(html, 'lxml')

    tweet = tweet_soup.find('p', class_='TweetTextSize')

    weather_tweet = tweet.get_text()
#unindenting in case of Python indentation weirdness
#the following code was used to scrap static information to mongo, since it
#overloads heroku. This was done and stored in mongodb, but cannot be rerun
#from heroku

'''
#Scrape for Mars trivia/facts
url = 'https://space-facts.com/mars/'
html = browser.html
fact_soup = bs(html, 'lxml')

browser.visit(url)
facts_table = fact_soup.find_all(id='tablepress-mars')

for fact in facts_table:
fact_data=(fact.text)
print(fact_data)

#convert to df for converting to html table
df = pd.read_html('https://space-facts.com/mars/')[0]
df.columns = ['Description', 'Factoid']
df.set_index('Description', inplace=True)
table = df.to_html()
facts_table = table.replace('\n', '')

#Well, that was an interesting circle to run around ^^^

#Grab Mars hemispheres photos
url ='https://astrogeology.usgs.gov/search/results?q=hemisphere+enhanced&k1=target&v1=Mars'

browser.visit(url)
html = browser.html
hemi_soup = bs(html,'lxml')

hemi = hemi_soup.find_all('a', class_='itemLink')

#Random sleeps for clicking. Several pages to click. Return full jpg URL
hemispheres = []
for i in hemi[1::2]:
browser.visit('https://astrogeology.usgs.gov'+i['href'])
time.sleep(random.random()*6)
full_image = browser.find_by_text('Sample').first
time.sleep(random.random()*1)
hemispheres.append(full_image['href'])
browser.back()
time.sleep(random.random()*4)
'''

    #set up dictionary for flask
    mars['headline'] = latest_title
    mars['paragraph_text'] = paragraph
    mars['featured_image'] = featured_image_link
    mars['weather'] = weather_tweet
#mars['facts'] = facts_table
#mars['hemispheres'] = hemispheres

    browser.quit()

    return mars
